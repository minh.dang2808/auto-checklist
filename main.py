import os
import sys
from multiprocessing.spawn import freeze_support
from PyQt5.QtWidgets import QMainWindow, QFileDialog, QMessageBox
from PyQt5 import QtCore, QtWidgets, uic
from PyQt5.QtCore import pyqtSlot, QRunnable, QObject, QThreadPool
from functools import partial
import openpyxl
from openpyxl.utils import get_column_letter
import concurrent
from concurrent.futures import ThreadPoolExecutor


def execute_validators(file_path):
    workbook = openpyxl.load_workbook(file_path)

    output_name = file_path.split('/')[-1]
    all_checklist = []
    if ui.radio_pd and ui.radio_pd.isChecked():
        from validators.PD.all_sheet_validator import AllSheetValidator
        from validators.PD.cover_sheet_validator import CoverSheetValidator
        from validators.PD.history_sheet_validator import HistorySheetValidator
        from validators.PD.diagram_sheet_validator import DiagramSheetValidator
        from validators.PD.screen_sheet_validator import ScreenSheetValidator
        from validators.PD.screen__item_sheet_validator import ScreenItemSheetValidator
        from validators.PD.process_validator import ProcessSheetValidator
        from validators.PD.service_sheet_validator import ServiceSheetValidator
        from validators.PD.sql_sheet_validator import SQLSheetValidator
        from validators.PD.workmodel_sheet_validator import WorkModelSheetValidator
        from validators.PD.remarks_sheet_validator import RemarksSheetValidator

        validators = [
            AllSheetValidator(),
            CoverSheetValidator(),
            HistorySheetValidator(),
            DiagramSheetValidator(),
            ScreenSheetValidator(),
            ScreenItemSheetValidator(),
            ProcessSheetValidator(),
            ServiceSheetValidator(),
            SQLSheetValidator(),
            WorkModelSheetValidator(),
            RemarksSheetValidator()
        ]

        for validator in validators:
            checklist = validator.validate(workbook)
            all_checklist.extend(checklist)
    elif ui.radio_scenario and ui.radio_scenario.isChecked():
        from validators.scenario.all_sheet_validator import AllSheetValidator
        from validators.scenario.cover_sheet_validator import CoverSheetValidator
        from validators.scenario.history_sheet_validator import HistorySheetValidator
        from validators.scenario.tc_and_result_sheet_validator import TcAndResultSheetValidator
        from validators.scenario.report_sheet_validator import ReportSheetValidator
        from validators.scenario.resolve_sheet_validator import ResolveSheetValidator

        validators = [
            AllSheetValidator(),
            CoverSheetValidator(),
            HistorySheetValidator(),
            TcAndResultSheetValidator(),
            ReportSheetValidator(),
            ResolveSheetValidator(),
        ]

        for validator in validators:
            checklist = validator.validate(workbook, output_name)
            all_checklist.extend(checklist)
    elif ui.radio_result and ui.radio_result.isChecked():
        from validators.result.all_sheet_validator import AllSheetValidator
        from validators.result.cover_sheet_validator import CoverSheetValidator
        from validators.result.history_sheet_validator import HistorySheetValidator
        from validators.result.category_sheet_validator import CategorySheetValidator
        from validators.result.evidence_sheet_validator import EvidenceSheetValidator

        validators = [
            AllSheetValidator(),
            CoverSheetValidator(),
            HistorySheetValidator(),
            CategorySheetValidator(),
        ]

        for validator in validators:
            checklist = validator.validate(workbook, output_name)
            all_checklist.extend(checklist)

        if "Danh mục" in workbook.sheetnames:
            for item in workbook.sheetnames:
                if item != 'Cover' and item != 'Lịch sử thay đổi' and item != 'Danh mục':
                    checklist = EvidenceSheetValidator().validate(workbook, item)
                    all_checklist.extend(checklist)

    save_to_excel(f"data\\checked_{output_name}", all_checklist)


def save_to_excel(output_file, checklist):
    output_workbook = openpyxl.Workbook()
    output_sheet = output_workbook.active

    output_sheet.append(["Sheet", "Item", "Result"])
    for item in checklist:
        output_sheet.append([item["sheet"], item["item"], "OK" if item["result"] else "Fail"])

    for column in output_sheet.columns:
        max_length = 0
        column_letter = get_column_letter(column[0].column)
        for cell in column:
            if cell.value:
                max_length = max(max_length, len(str(cell.value)))
        adjusted_width = (max_length + 2) * 1.2
        output_sheet.column_dimensions[column_letter].width = adjusted_width

    output_workbook.save(output_file)


def open_folder(file_name):
    os.system(f"start {file_name}")


def select_file():
    filenames, _ = QFileDialog.getOpenFileNames(
        None,
        "Select Files",
        "",
        "Text Files (*.xlsx)",
    )

    if filenames:
        ui.logs.clear()
        for filename in filenames:
            ui.logs.append(filename)


def send_with_thread_executor(max_workers, files_path):
    with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
        futures = []
        for file_path in [item for item in files_path if item]:
            futures.append(
                executor.submit(
                    execute_validators, file_path
                )
            )


class WorkerSignals(QObject):
    finished = QtCore.pyqtSignal()  # create a signal
    result = QtCore.pyqtSignal(object)  # create a signal that gets an object as argument


class Worker(QRunnable):
    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()
        self.fn = fn  # Get the function passed in
        self.args = args  # Get the arguments passed in
        self.kwargs = kwargs  # Get the keyward arguments passed in
        self.signals = WorkerSignals()  # Create a signal class

    @pyqtSlot()
    def run(self):  # our thread's worker function
        result = self.fn(*self.args, **self.kwargs)  # execute the passed in function with its arguments
        self.signals.result.emit(result)  # return result
        self.signals.finished.emit()  # emit when thread ended


def thread_finished():
    ui.start.setEnabled(True)
    ui.start.setText('Start')

    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)
    msg.setText("Test process completed.")
    msg.setWindowTitle("Message")
    msg.exec_()
    print("Finished signal emitted.")


class Ui_MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):  # -----------------------------------------
        super(Ui_MainWindow, self).__init__(*args, **kwargs)  # |
        uic.loadUi('auto_check.ui', self)

        self.threadpool = QThreadPool()
        self.threadpool.setMaxThreadCount(1)

        self.logs = self.findChild(QtWidgets.QTextBrowser, 'logs')

        self.start = self.findChild(QtWidgets.QPushButton, 'start')
        self.start.pressed.connect(self.threadRunner)

        self.open_folder = self.findChild(QtWidgets.QPushButton, 'open_folder')
        self.open_folder.clicked.connect(partial(open_folder, "data"))

        self.select_file = self.findChild(QtWidgets.QPushButton, 'select_file')
        self.select_file.clicked.connect(select_file)

        self.radio_pd = self.findChild(QtWidgets.QRadioButton, 'radio_pd_2')
        self.radio_scenario = self.findChild(QtWidgets.QRadioButton, 'radio_scenario')
        self.radio_result = self.findChild(QtWidgets.QRadioButton, 'radio_result')

        self.thread_count = self.findChild(QtWidgets.QSpinBox, 'thread_count')

        self.show()

    def generate(self):
        try:
            self.start.setEnabled(False)
            self.start.setText('Running')

            num_worker = int(self.thread_count.value())
            files_path = self.logs.toPlainText().split('\n')

            send_with_thread_executor(num_worker, files_path)
        except Exception as e:
            print(e)

    def threadRunner(self):
        try:
            worker = Worker(self.generate)  # create our thread and give it a function as argument with its args
            worker.signals.finished.connect(thread_finished)  # connect finish signal of our thread to thread_complete
            self.threadpool.start(worker)  # start thread
        except Exception as e:
            print(e)


if __name__ == '__main__':
    freeze_support()
    app = QtWidgets.QApplication(sys.argv)
    ui = Ui_MainWindow()
    sys.exit(app.exec_())
