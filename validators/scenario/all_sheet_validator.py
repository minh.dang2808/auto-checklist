import re

from validators.base_validator import BaseValidator


class AllSheetValidator(BaseValidator):
    def validate(self, workbook, file_name):
        required_sheets = [
            "Cover",
            "Lịch sử thay đổi",
            "Danh sách test cases & Kết quả",
            "Bảng phân tích lỗi",
            "Bảng quyết định",
        ]

        checklist = [
            {
                "sheet": "ALL",
                "item": "Tên file có định dạng 単体テスト仕様書_ScreenID_ScreenName_version_VN.xlxs hay chưa",
                "result": self.is_valid_filename(file_name)
            }
        ]

        for sheet_name in required_sheets:
            result = {
                "sheet": "ALL",
                "item": "Có sheet " + sheet_name + " hay chưa?",
                "result": False
            }

            if sheet_name in workbook.sheetnames:
                result["result"] = True
            checklist.append(result)

        return checklist

    def is_valid_filename(self, file_name):
        pattern = r'^単体テスト仕様書_[A-Z0-9]+_.*_v\d+\.\d+_VN\.xlsx$'

        if re.match(pattern, file_name):
            return True
        else:
            return False
