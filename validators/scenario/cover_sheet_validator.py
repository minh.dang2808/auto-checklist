from validators.base_validator import BaseValidator


class CoverSheetValidator(BaseValidator):
    def validate(self, workbook, file_name):
        sheet_name = "Cover"
        if sheet_name not in workbook.sheetnames:
            return []

        sheet = workbook[sheet_name]
        checklist = self.tc_1(sheet)
        checklist += self.tc_2(sheet)
        checklist += self.tc_3(sheet)
        checklist += self.tc_4(sheet)
        checklist += self.tc_5(sheet)

        for item in checklist:
            item['sheet'] = sheet_name

        return checklist

    def tc_1(self, sheet):
        checklist = [
            {"item": "Ô cell A6: font ＭＳ ゴシック hay chưa?", "result": False},
            {"item": "Ô cell A6: size 36 hay chưa?", "result": False},
        ]

        if sheet['A6'].font.name == 'ＭＳ ゴシック':
            checklist[0]["result"] = True

        if sheet['A6'].font.size == 36:
            checklist[1]["result"] = True

        return checklist

    def tc_2(self, sheet):
        cell = sheet["A24"]
        checklist = [
            {"item": "Cell A24: Đã có reference đến sheet 'Lịch sử thay đổi' hay chưa?", "result": False},
        ]

        if cell.value in '=IF(COUNTA(\'Lịch sử thay đổi\'!C3:E32)>0,"Phiên bản "&OFFSET(\'Lịch sử thay đổi\'!C3,COUNTA(\'Lịch sử thay đổi\'!C3:E32)-1,0),"Phiên bản 0.0")':
            checklist[0]["result"] = True

        return checklist

    def tc_3(self, sheet):
        cell = sheet["A26"]
        checklist = [
            {"item": "Cell A26: Đã có reference đến sheet 'Lịch sử thay đổi' hay chưa?", "result": False},
            {"item": "Cell A26 đã có nội dung là 「YYYY/MM/DD」 hay chưa?", "result": False},
        ]

        if cell.value in '=IF(COUNTA(\'Lịch sử thay đổi\'!F3:J32)>0,OFFSET(\'Lịch sử thay đổi\'!F3,COUNTA(\'Lịch sử thay đổi\'!F3:J32)-1,0),"1900/01/01")':
            checklist[0]["result"] = True
            checklist[1]["result"] = True

        return checklist

    def tc_4(self, sheet):
        cell = sheet["A28"]
        checklist = [
            {"item": "Ô cell A28 đã có tên khách hàng chưa?", "result": False},
        ]

        if 'BIPROGY株式会社' in cell.value:
            checklist[0]["result"] = True

        return checklist

    def tc_5(self, sheet):
        header_row = sheet[31]
        required_columns = ["承認", "審査", "作成"]
        checklist = [
            {"item": "Ô cell AZ31 đã có cột 承認 chưa?", "result": False},
            {"item": "Ô cell BD31 đã có cột 審査 chưa?", "result": False},
            {"item": "Ô cell BH31 đã có cột 作成 chưa?", "result": False},
        ]

        if "承認" in header_row[51].value:
            checklist[0]["result"] = True

        if "審査" in header_row[55].value:
            checklist[1]["result"] = True

        if "作成" in header_row[59].value:
            checklist[2]["result"] = True

        return checklist
