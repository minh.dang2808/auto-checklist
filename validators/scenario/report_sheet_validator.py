from validators.base_validator import BaseValidator


class ReportSheetValidator(BaseValidator):
    def validate(self, workbook, file_name):
        sheet_name = "Bảng phân tích lỗi"
        if sheet_name not in workbook.sheetnames:
            return []

        sheet = workbook[sheet_name]
        checklist = self.tc_1(sheet)
        checklist += self.tc_2(sheet)
        checklist += self.tc_3(sheet)
        checklist += self.tc_4(sheet)

        for item in checklist:
            item['sheet'] = sheet_name

        return checklist

    def tc_1(self, sheet):
        checklist = [
            {"item": "Font chữ Times New Roman", "result": False},
            {"item": "A1: Font size 24", "result": False},
        ]

        if sheet['A1'].font.name == 'Times New Roman':
            checklist[0]["result"] = True

        if sheet['A1'].font.size == 24:
            checklist[1]["result"] = True

        return checklist

    def tc_2(self, sheet):
        checklist = [
            {"item": "C5 đã có thông tin chưa, có chính xác không", "result": False},
            {"item": "F3 đã có thông tin chính xác chưa, có chính xác không?", "result": False},
            {"item": "F5 đã có thông tin và điền bằng tiếng nhật hay chưa", "result": False},
        ]

        if sheet['C5'].value:
            checklist[0]["result"] = True

        if sheet['F3'].value:
            checklist[1]["result"] = True

        if sheet['F5'].value:
            checklist[2]["result"] = True

        return checklist

    def tc_3(self, sheet):
        checklist = [
            {"item": "I3, I4, L3, L4 đã có thông tin chưa?", "result": False},
            {"item": "Bảng danh sách test case đã có đủ 3 cột hay chưa?", "result": False},
            {"item": "Các thông tin của các cột đã được nhập hay chưa?", "result": False},
            {"item": "Kiểm tra bảng phân tích lỗi đã đủ 14 cột hay chưa (không tính các cột con)", "result": False},
            {"item": "Kiểm tra ô F7 đã được chia thành 3 phân loại lỗi chưa", "result": False},
            {"item": "Kiểm tra ô F8 đã được chia thành 4 loại lỗi chưa", "result": False},
            {"item": "Kiểm tra ô J8 đã được chia thành 4 loại lỗi chưa", "result": False},
            {"item": "Kiểm tra ô N8 đã được chia thành 3 loại lỗi chưa", "result": False},
            {"item": "Kiểm tra ô Q7 đã chia thành 5 cột tương ứng với 5 nguyên nhân phát sinh chưa", "result": False},
            {"item": "Kiểm tra ô V7 đã chia thành 6 cột tương ứng với 6 giai đoạn trong phát triển phần mềm chưa", "result": False},
        ]

        if sheet[f"I3"].value and sheet[f"I4"].value and sheet[f"L3"].value and sheet[f"L4"].value:
            checklist[0]["result"] = True

        if sheet[f"F7"].value and sheet[f"Q7"].value and sheet[f"L3"].value and sheet[f"V7"].value:
            checklist[1]["result"] = True
            checklist[2]["result"] = True

        if sheet[f"A7"].value and sheet[f"B7"].value and sheet[f"C7"].value and sheet[f"D7"].value and \
                sheet["E7"] and sheet[f"F7"].value and sheet[f"Q7"].value and sheet[f"V7"].value and \
                sheet[f"AB7"].value and sheet[f"AC7"].value and sheet[f"AD7"].value and sheet[f"AE7"].value and \
                sheet[f"AF7"].value and sheet[f"AG7"].value:
            checklist[3]["result"] = True

        if sheet[f"F8"].value and sheet[f"J8"].value and sheet[f"N8"].value:
            checklist[4]["result"] = True

        if sheet[f"F9"].value and sheet[f"G9"].value and sheet[f"H9"].value and sheet[f"I9"]:
            checklist[5]["result"] = True

        if sheet[f"J9"].value and sheet[f"K9"].value and sheet[f"L9"].value and sheet[f"M9"]:
            checklist[6]["result"] = True

        if sheet[f"N9"].value and sheet[f"O9"].value and sheet[f"P9"].value:
            checklist[6]["result"] = True

        if sheet[f"Q9"].value and sheet[f"R9"].value and sheet[f"S9"].value and sheet[f"T9"].value and sheet[f"U9"].value:
            checklist[7]["result"] = True

        if sheet[f"V9"].value and sheet[f"W9"].value and sheet[f"X9"].value and sheet[f"Y9"].value and sheet[f"Z9"].value and sheet[f"AA9"].value:
            checklist[8]["result"] = True

        return checklist

    def tc_4(self, sheet):
        checklist = [
            {"item": "Kiểm tra nội dung các cột A7, B7, C7, D7, E7 đã được điền hết chưa", "result": False},
            {"item": "Kiểm tra mỗi bug đã được điền số 1 để đánh dấu phân loại lỗi hay chưa", "result": True},
            {"item": "Kiểm tra mỗi bug đã được điền số 1 để đánh dấu phân loại nguyên nhân hay chưa", "result": True},
            {"item": "Kiểm tra mỗi bug đã được điền số 1 để đánh dấu phân loại giai đoạn phát sinh hay chưa", "result": True},
            {"item": "Kiểm tra xem các cột từ AB -> AF đã được điền đầy đủ và chính xác hay chưa", "result": True},
        ]

        size = 0
        result_1 = 0
        result_2 = 0
        for i in range(10, 100):
            if sheet[f"A{i}"].value and sheet[f"B{i}"].value:
                size += 1
                if sheet[f"C{i}"].value and sheet[f"D{i}"].value and sheet[f"E{i}"].value:
                    result_1 += 1
                if sheet[f"AB{i}"].value and sheet[f"AC{i}"].value and sheet[f"AD{i}"].value and sheet[f"AE{i}"].value \
                        and sheet[f"AF{i}"].value:
                    result_2 += 1

        checklist[0]["result"] = bool(result_1 == size)
        checklist[4]["result"] = bool(result_2 == size)

        return checklist
