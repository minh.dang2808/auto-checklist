from validators.base_validator import BaseValidator


class ResolveSheetValidator(BaseValidator):
    def validate(self, workbook, file_name):
        sheet_name = "Bảng quyết định"
        if sheet_name not in workbook.sheetnames:
            return []

        sheet = workbook[sheet_name]
        checklist = self.tc_1(sheet)
        checklist += self.tc_2(sheet)
        checklist += self.tc_3(sheet)

        for item in checklist:
            item['sheet'] = sheet_name

        return checklist

    def tc_1(self, sheet):
        checklist = [
            {"item": "Font chữ Times New Roman", "result": False},
            {"item": "A1: Font size 24", "result": False},
        ]

        if sheet['A1'].font.name == 'Times New Roman':
            checklist[0]["result"] = True

        if sheet['A1'].font.size == 24:
            checklist[1]["result"] = True

        return checklist

    def tc_2(self, sheet):
        checklist = [
            {"item": "C5 đã có thông tin chưa", "result": False},
            {"item": "E3, E5 đã có thông tin chưa", "result": False},
        ]

        if sheet['C5'].value:
            checklist[0]["result"] = True

        if sheet['E3'].value and sheet['E5'].value:
            checklist[1]["result"] = True

        return checklist

    def tc_3(self, sheet):
        checklist = [
            {"item": "L3, L4, S3, S4 đã có thông tin chưa?", "result": False},
            {"item": "Kiểm tra xem bảng quyết định đã có đủ 3 phần (Screen Input, Interaction, Kết quả kì vọng)?", "result": False},
        ]

        if sheet[f"L3"].value and sheet[f"L4"].value and sheet[f"S3"].value and sheet[f"S4"].value:
            checklist[0]["result"] = True

        check = 0
        for item in sheet['A8:A100']:
            if item[0].value and ('Screen input' in item[0].value or 'Interaction' in item[0].value or 'Kết quả kì vọng' in item[0].value):
                check += 1
        checklist[1]["result"] = bool(check == 3)

        return checklist
