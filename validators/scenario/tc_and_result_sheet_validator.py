from validators.base_validator import BaseValidator


class TcAndResultSheetValidator(BaseValidator):
    def validate(self, workbook, file_name):
        sheet_name = "Danh sách test cases & Kết quả"
        if sheet_name not in workbook.sheetnames:
            return []

        sheet = workbook[sheet_name]
        checklist = self.tc_1(sheet)
        checklist += self.tc_2(sheet)
        checklist += self.tc_3(sheet)
        checklist += self.tc_4(sheet)
        checklist += self.tc_5(sheet)

        for item in checklist:
            item['sheet'] = sheet_name

        return checklist

    def tc_1(self, sheet):
        checklist = [
            {"item": "Font chữ Times New Roman", "result": False},
            {"item": "A1: Font size 24", "result": False},
        ]

        if sheet['A1'].font.name == 'Times New Roman':
            checklist[0]["result"] = True

        if sheet['A1'].font.size == 24:
            checklist[1]["result"] = True

        return checklist

    def tc_2(self, sheet):
        checklist = [
            {"item": "C5 đã có thông tin chưa, có chính xác không", "result": False},
            {"item": "F3 đã có thông tin chính xác chưa, có chính xác không?", "result": False},
            {"item": "F5 đã có thông tin và điền bằng tiếng nhật hay chưa", "result": False},
        ]

        if sheet['C5'].value:
            checklist[0]["result"] = True

        if sheet['F3'].value:
            checklist[1]["result"] = True

        if sheet['F5'].value:
            checklist[2]["result"] = True

        return checklist

    def tc_3(self, sheet):
        checklist = [
            {"item": "I3, I4, K3, K4 đã có thông tin chưa?", "result": False},
            {"item": "Bảng danh sách test case đã có đủ 18 cột hay chưa?", "result": False},
        ]

        if sheet[f"I3"].value and sheet[f"I4"].value and sheet[f"K3"].value and sheet[f"K4"].value:
            checklist[0]["result"] = True

        if sheet[f"A7"].value and sheet[f"B7"].value and sheet[f"C7"].value and sheet[f"D7"].value and \
                sheet["E7"] and sheet[f"F7"].value and sheet[f"G7"].value and sheet[f"H7"].value and \
                sheet[f"I7"].value and sheet[f"J7"].value and sheet[f"K7"].value and sheet[f"L7"].value and \
                sheet[f"M7"].value and sheet[f"N7"].value and sheet[f"O7"].value and sheet[f"P7"].value and \
                sheet[f"Q7"].value and sheet[f"R7"].value:
            checklist[1]["result"] = True

        return checklist

    def tc_4(self, sheet):
        checklist = [
            {"item": "Các thông tin của các cột A7, B7, C7, D7 đã được nhập đủ chưa", "result": False},
        ]

        size = 0
        result = 0
        for i in range(8, 100):
            if sheet[f"A{i}"].value:
                size += 1
                if sheet[f"B{i}"].value and sheet[f"C{i}"].value and sheet[f"D{i}"].value:
                    result += 1

        checklist[0]["result"] = bool(result == size)

        return checklist

    def tc_5(self, sheet):
        checklist = [
            {"item": "Nội dung của cột E7 (Phân loại 3) có hay không có thông tin, nếu không có thì kiểm tra xem "
                     "có bị thiếu thông tin của cột đó không", "result": False},
            {"item": "Nội dung của cột G7 (Kiểu) đã điền đủ thông tin (Bình thường hay Bất thường) chưa", "result": False},
            {"item": "Nội dung cột I7 đã điền hết thông tin chưa, với mỗi test thì kiểm tra xem còn thiếu điều kiện gì nữa không?", "result": False},
            {"item": "Nội dung cột J7 đã điền hết thông tin hay chưa?", "result": False},
            {"item": "Nội dung cột K7 đã điền hết thông tin chưa và còn thiếu bước nào nữa không?", "result": False},
            {"item": "Từ cột L -> cột Q đã được nhập hết thông tin chính xác hay chưa?", "result": False},
        ]

        size = 0
        result_1 = 0
        result_2 = 0
        result_3 = 0
        result_4 = 0
        result_5 = 0
        result_6 = 0
        for i in range(8, 100):
            if sheet[f"A{i}"].value:
                size += 1
                if sheet[f"E{i}"].value:
                    result_1 += 1
                if sheet[f"G{i}"].value and ("Bình thường" in sheet[f"G{i}"].value or "Bất thường" in sheet[f"G{i}"].value):
                    result_2 += 1
                if sheet[f"I{i}"].value:
                    result_3 += 1
                if sheet[f"J{i}"].value:
                    result_4 += 1
                if sheet[f"K{i}"].value:
                    result_5 += 1
                if sheet[f"L{i}"].value and sheet[f"M{i}"].value and sheet[f"N{i}"].value and sheet[f"O{i}"].value and sheet[f"P{i}"].value and sheet[f"Q{i}"].value and sheet[f"R{i}"].value:
                    result_6 += 1

        checklist[0]["result"] = bool(result_1 == size)
        checklist[1]["result"] = bool(result_2 == size)
        checklist[2]["result"] = bool(result_3 == size)
        checklist[3]["result"] = bool(result_4 == size)
        checklist[4]["result"] = bool(result_5 == size)
        checklist[5]["result"] = bool(result_6 == size)

        return checklist
