from validators.base_validator import BaseValidator


class AllSheetValidator(BaseValidator):
    def validate(self, workbook):
        required_sheets = [
            "表紙",
            "改訂履歴",
            "画面遷移図",
            "画面レイアウト",
            "画面項目",
            "処理",
            "Service",
            "SQL",
            "WorkModel",
            "備考",
        ]

        checklist = []
        for sheet_name in required_sheets:
            result = {
                "sheet": "ALL",
                "item": "Có sheet " + sheet_name + " hay chưa?",
                "result": False
            }

            if sheet_name in workbook.sheetnames:
                result["result"] = True
            checklist.append(result)

        return checklist
