from validators.base_validator import BaseValidator


class SQLSheetValidator(BaseValidator):
    def validate(self, workbook):
        sheet_name = "SQL"
        if sheet_name not in workbook.sheetnames:
            return []

        sheet = workbook[sheet_name]
        checklist = [self.tc_1(sheet)]
        checklist += self.tc_2(sheet)
        checklist += self.tc_3(sheet)
        checklist += self.tc_4(sheet)
        checklist += self.tc_5(sheet)
        checklist += self.tc_6(sheet)

        for item in checklist:
            item['sheet'] = sheet_name

        return checklist

    def tc_1(self, sheet):
        result = False
        if sheet['A1'].font.name == 'ＭＳ Ｐゴシック':
            result = True

        return {
            "item": "Font của cả sheet này đã là 「ＭＳ Ｐゴシック」 hay chưa?",
            "result": result
        }

    def tc_2(self, sheet):
        row = 1
        cell = sheet[f"A{row}"]
        checklist = [
            {"item": f"Cell A{row} đã có text là 「SQL」hay chưa?", "result": False},
            {"item": f"Cell A{row} đã có font size là 18 hay chưa?", "result": False},
            {"item": f"Cell A{row} đã có font là in đậm hay chưa?", "result": False},
            {"item": f"Cell A{row} text đã ở vị trí center hay chưa?", "result": False},
            {"item": f"Cell A{row} đã merge các cột A -> BL vào hay chưa?", "result": False},
            {"item": f"Cell A{row} đã 「Thick Outside Borders」hay chưa?", "result": False},
        ]

        if "SQL" in cell.value:
            checklist[0]["result"] = True
        if cell.font.size == 18:
            checklist[1]["result"] = True
        if cell.font.b:
            checklist[2]["result"] = True
        if cell.alignment.horizontal == "center":
            checklist[3]["result"] = True
        if f'A{row}:BL{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True
        if cell.border.bottom.style and cell.border.right.style:
            checklist[5]["result"] = True

        return checklist

    def tc_3(self, sheet):
        row = 3
        header_row = sheet[row]

        checklist = [
            {"item": f"Dòng {row}, đã có text「サブシステムID」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> F vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột G -> N vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「サブシステム名」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột O -> T vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột U -> AJ vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「作成者」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AK -> AP vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột AQ -> AX vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「作成日」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AY -> BD vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột BE -> BL vào hay chưa?", "result": False},
        ]

        if header_row[0].value and "サブシステムID" in header_row[0].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if f'A{row}:F{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[0].fill.start_color.index == 22 or header_row[0].fill.start_color.index == 'FFC0C0C0':
            checklist[3]["result"] = True
        if f'G{row}:N{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True

        if header_row[14].value and "サブシステム名" in header_row[14].value:
            checklist[5]["result"] = True
            checklist[6]["result"] = True
        if f'O{row}:T{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[7]["result"] = True
        if header_row[14].fill.start_color.index == 22 or header_row[14].fill.start_color.index == 'FFC0C0C0':
            checklist[8]["result"] = True
        if f'U{row}:AJ{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[9]["result"] = True

        if header_row[36].value and "作成者" in header_row[36].value:
            checklist[10]["result"] = True
            checklist[11]["result"] = True
        if f'AK{row}:AP{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[12]["result"] = True
        if header_row[36].fill.start_color.index == 22 or header_row[36].fill.start_color.index == 'FFC0C0C0':
            checklist[13]["result"] = True
        if f'AQ{row}:AX{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[14]["result"] = True

        if header_row[50].value and "作成日" in header_row[50].value:
            checklist[15]["result"] = True
            checklist[16]["result"] = True
        if f'AY{row}:BD{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[17]["result"] = True
        if header_row[50].fill.start_color.index == 22 or header_row[50].fill.start_color.index == 'FFC0C0C0':
            checklist[18]["result"] = True
        if f'BE{row}:BL{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[19]["result"] = True

        return checklist

    def tc_4(self, sheet):
        row = 4
        header_row = sheet[row]

        checklist = [
            {"item": f"Dòng {row}, đã có text「管理ID」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> F vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột G -> N vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「サービス群名」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột O -> T vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột U -> AJ vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「更新者」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AK -> AP vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột AQ -> AX vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「更新日」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AY -> BD vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột BE -> BL vào hay chưa?", "result": False},
        ]

        if header_row[0].value and "管理ID" in header_row[0].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if f'A{row}:F{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[0].fill.start_color.index == 22 or header_row[0].fill.start_color.index == 'FFC0C0C0':
            checklist[3]["result"] = True
        if f'G{row}:N{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True

        if header_row[14].value and "サービス群名" in header_row[14].value:
            checklist[5]["result"] = True
            checklist[6]["result"] = True
        if f'O{row}:T{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[7]["result"] = True
        if header_row[14].fill.start_color.index == 22 or header_row[14].fill.start_color.index == 'FFC0C0C0':
            checklist[8]["result"] = True
        if f'U{row}:AJ{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[9]["result"] = True

        if header_row[36].value and "更新者" in header_row[36].value:
            checklist[10]["result"] = True
            checklist[11]["result"] = True
        if f'AK{row}:AP{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[12]["result"] = True
        if header_row[36].fill.start_color.index == 22 or header_row[36].fill.start_color.index == 'FFC0C0C0':
            checklist[13]["result"] = True
        if f'AQ{row}:AX{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[14]["result"] = True

        if header_row[50].value and "更新日" in header_row[50].value:
            checklist[15]["result"] = True
            checklist[16]["result"] = True
        if f'AY{row}:BD{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[17]["result"] = True
        if header_row[50].fill.start_color.index == 22 or header_row[50].fill.start_color.index == 'FFC0C0C0':
            checklist[18]["result"] = True
        if f'BE{row}:BL{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[19]["result"] = True

        return checklist

    def tc_5(self, sheet):
        row = 5
        header_row = sheet[row]

        checklist = [
            {"item": f"Dòng {row}, đã có text「サービスID」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> F vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột G -> N vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「サービス名」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột O -> T vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột U -> AJ vào hay chưa?", "result": False},
        ]

        if header_row[0].value and "サービスID" in header_row[0].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if f'A{row}:F{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[0].fill.start_color.index == 22 or header_row[0].fill.start_color.index == 'FFC0C0C0':
            checklist[3]["result"] = True
        if f'G{row}:N{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True

        if header_row[14].value and "サービス名" in header_row[14].value:
            checklist[5]["result"] = True
            checklist[6]["result"] = True
        if f'O{row}:T{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[7]["result"] = True
        if header_row[14].fill.start_color.index == 22 or header_row[14].fill.start_color.index == 'FFC0C0C0':
            checklist[8]["result"] = True
        if f'U{row}:AJ{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[9]["result"] = True

        return checklist

    def tc_6(self, sheet):
        row = 7
        header_row = sheet[row]

        checklist = [
            {"item": f"Dòng {row}, đã có text「画面ID」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> F vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột G -> AE vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#FFCC99」 hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「画面名」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AF -> AK vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột AL -> BL vào hay chưa?", "result": False},
        ]

        if header_row[0].value and "画面ID" in header_row[0].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if f'A{row}:F{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[0].fill.start_color.index == 22 or header_row[0].fill.start_color.index == 'FFC0C0C0':
            checklist[3]["result"] = True
        if f'G{row}:AE{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True
        if header_row[6].fill.start_color.index == 47:
            checklist[5]["result"] = True

        if header_row[31].value and "画面名" in header_row[31].value:
            checklist[6]["result"] = True
            checklist[7]["result"] = True
        if f'AF{row}:AK{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[8]["result"] = True
        if header_row[31].fill.start_color.index == 22 or header_row[31].fill.start_color.index == 'FFC0C0C0':
            checklist[9]["result"] = True
        if f'AL{row}:BL{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[10]["result"] = True

        return checklist
