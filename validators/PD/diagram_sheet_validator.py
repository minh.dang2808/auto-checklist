from validators.base_validator import BaseValidator


class DiagramSheetValidator(BaseValidator):
    def validate(self, workbook):
        sheet_name = "画面遷移図"
        if sheet_name not in workbook.sheetnames:
            return []

        sheet = workbook[sheet_name]
        checklist = [self.tc_1(sheet)]
        checklist += self.tc_2(sheet)
        checklist += self.tc_3(sheet)
        checklist += self.tc_4(sheet)
        checklist += self.tc_5(sheet)

        for item in checklist:
            item['sheet'] = sheet_name

        return checklist

    def tc_1(self, sheet):
        result = False
        if sheet['A1'].font.name == 'ＭＳ Ｐゴシック':
            result = True

        return {
            "item": "Font của cả sheet này đã là 「ＭＳ Ｐゴシック」 hay chưa?",
            "result": result
        }

    def tc_2(self, sheet):
        cell = sheet["A1"]
        checklist = [
            {"item": "Cell A1 đã có text là 「画面遷移図」hay chưa?", "result": False},
            {"item": "Cell A1 đã có font size là 18 hay chưa?", "result": False},
            {"item": "Cell A1 đã có font là in đậm hay chưa?", "result": False},
            {"item": "Cell A1 text đã ở vị trí center hay chưa?", "result": False},
            {"item": "Cell A1 đã merge các cột A -> BL vào hay chưa?", "result": False},
            {"item": "Cell A1 đã 「Thick Outside Borders」hay chưa?", "result": False},
        ]

        if "画面遷移図" in cell.value:
            checklist[0]["result"] = True
        if cell.font.size == 18:
            checklist[1]["result"] = True
        if cell.font.b:
            checklist[2]["result"] = True
        if cell.alignment.horizontal == "center":
            checklist[3]["result"] = True
        if 'A1:BL1' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True
        if cell.border.bottom.style and cell.border.right.style:
            checklist[5]["result"] = True

        return checklist

    def tc_3(self, sheet):
        header_row = sheet[3]

        checklist = [
            {"item": "Dòng 3, đã có text「サブシステムID」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> F vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": "Dòng 3, đã merge các cột G -> N vào hay chưa?", "result": False},

            {"item": "Dòng 3, đã có text「サブシステム名」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột O -> T vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": "Dòng 3, đã merge các cột U -> AJ vào hay chưa?", "result": False},

            {"item": "Dòng 3, đã có text「作成者」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AK -> AP vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": "Dòng 3, đã merge các cột AQ -> AX vào hay chưa?", "result": False},

            {"item": "Dòng 3, đã có text「作成日」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AY -> BD vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": "Dòng 3, đã merge các cột BE -> BL vào hay chưa?", "result": False},
        ]

        if header_row[0].value and "サブシステムID" in header_row[0].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if 'A3:F3' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[0].fill.start_color.index == 22:
            checklist[3]["result"] = True
        if 'G3:N3' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True

        if header_row[14].value and "サブシステム名" in header_row[14].value:
            checklist[5]["result"] = True
            checklist[6]["result"] = True
        if 'O3:T3' in str(list(sheet.merged_cells.ranges)):
            checklist[7]["result"] = True
        if header_row[14].fill.start_color.index == 22:
            checklist[8]["result"] = True
        if 'U3:AJ3' in str(list(sheet.merged_cells.ranges)):
            checklist[9]["result"] = True

        if header_row[36].value and "作成者" in header_row[36].value:
            checklist[10]["result"] = True
            checklist[11]["result"] = True
        if 'AK3:AP3' in str(list(sheet.merged_cells.ranges)):
            checklist[12]["result"] = True
        if header_row[36].fill.start_color.index == 22:
            checklist[13]["result"] = True
        if 'AQ3:AX3' in str(list(sheet.merged_cells.ranges)):
            checklist[14]["result"] = True

        if header_row[50].value and "作成日" in header_row[50].value:
            checklist[15]["result"] = True
            checklist[16]["result"] = True
        if 'AY3:BD3' in str(list(sheet.merged_cells.ranges)):
            checklist[17]["result"] = True
        if header_row[50].fill.start_color.index == 22:
            checklist[18]["result"] = True
        if 'BE3:BL3' in str(list(sheet.merged_cells.ranges)):
            checklist[19]["result"] = True

        return checklist

    def tc_4(self, sheet):
        header_row = sheet[4]

        checklist = [
            {"item": "Dòng 4, đã có text「管理ID」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> F vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": "Dòng 4, đã merge các cột G -> N vào hay chưa?", "result": False},

            {"item": "Dòng 4, đã có text「サービス群名」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột O -> T vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": "Dòng 4, đã merge các cột U -> AJ vào hay chưa?", "result": False},

            {"item": "Dòng 4, đã có text「更新者」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AK -> AP vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": "Dòng 4, đã merge các cột AQ -> AX vào hay chưa?", "result": False},

            {"item": "Dòng 4, đã có text「更新日」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AY -> BD vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": "Dòng 4, đã merge các cột BE -> BL vào hay chưa?", "result": False},
        ]

        if header_row[0].value and "管理ID" in header_row[0].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if 'A4:F4' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[0].fill.start_color.index == 22:
            checklist[3]["result"] = True
        if 'G4:N4' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True

        if header_row[14].value and "サービス群名" in header_row[14].value:
            checklist[5]["result"] = True
            checklist[6]["result"] = True
        if 'O4:T4' in str(list(sheet.merged_cells.ranges)):
            checklist[7]["result"] = True
        if header_row[14].fill.start_color.index == 22:
            checklist[8]["result"] = True
        if 'U4:AJ4' in str(list(sheet.merged_cells.ranges)):
            checklist[9]["result"] = True

        if header_row[36].value and "更新者" in header_row[36].value:
            checklist[10]["result"] = True
            checklist[11]["result"] = True
        if 'AK4:AP4' in str(list(sheet.merged_cells.ranges)):
            checklist[12]["result"] = True
        if header_row[36].fill.start_color.index == 22:
            checklist[13]["result"] = True
        if 'AQ4:AX4' in str(list(sheet.merged_cells.ranges)):
            checklist[14]["result"] = True

        if header_row[50].value and "更新日" in header_row[50].value:
            checklist[15]["result"] = True
            checklist[16]["result"] = True
        if 'AY4:BD4' in str(list(sheet.merged_cells.ranges)):
            checklist[17]["result"] = True
        if header_row[50].fill.start_color.index == 22:
            checklist[18]["result"] = True
        if 'BE4:BL4' in str(list(sheet.merged_cells.ranges)):
            checklist[19]["result"] = True

        return checklist

    def tc_5(self, sheet):
        header_row = sheet[5]

        checklist = [
            {"item": "Dòng 5, đã có text「サービスID」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> F vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": "Dòng 5, đã merge các cột G -> N vào hay chưa?", "result": False},

            {"item": "Dòng 5, đã có text「サービス名」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột O -> T vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": "Dòng 5, đã merge các cột U -> AJ vào hay chưa?", "result": False},
        ]

        if header_row[0].value and "サービスID" in header_row[0].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if 'A5:F5' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[0].fill.start_color.index == 22:
            checklist[3]["result"] = True
        if 'G5:N5' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True

        if header_row[14].value and "サービス名" in header_row[14].value:
            checklist[5]["result"] = True
            checklist[6]["result"] = True
        if 'O5:T5' in str(list(sheet.merged_cells.ranges)):
            checklist[7]["result"] = True
        if header_row[14].fill.start_color.index == 22:
            checklist[8]["result"] = True
        if 'U5:AJ5' in str(list(sheet.merged_cells.ranges)):
            checklist[9]["result"] = True

        return checklist
