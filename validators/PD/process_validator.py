from validators.base_validator import BaseValidator


class ProcessSheetValidator(BaseValidator):
    def validate(self, workbook):
        sheet_name = "処理"
        if sheet_name not in workbook.sheetnames:
            return []

        sheet = workbook[sheet_name]
        checklist = [self.tc_1(sheet)]
        checklist += self.tc_2(sheet)
        checklist += self.tc_3(sheet)
        checklist += self.tc_4(sheet)
        checklist += self.tc_5(sheet)
        checklist += self.tc_6(sheet)
        checklist += self.tc_7(sheet)
        checklist += self.tc_8(sheet)
        checklist += self.tc_9(sheet)
        checklist += self.tc_10(sheet)
        checklist += self.tc_11(sheet)
        checklist += self.tc_12(sheet)

        for item in checklist:
            item['sheet'] = sheet_name

        return checklist

    def tc_1(self, sheet):
        result = False
        if sheet['A1'].font.name == 'ＭＳ Ｐゴシック':
            result = True

        return {
            "item": "Font của cả sheet này đã là 「ＭＳ Ｐゴシック」 hay chưa?",
            "result": result
        }

    def tc_2(self, sheet):
        row = 1
        cell = sheet[f"A{row}"]
        checklist = [
            {"item": f"Cell A{row} đã có text là 「処理」hay chưa?", "result": False},
            {"item": f"Cell A{row} đã có font size là 18 hay chưa?", "result": False},
            {"item": f"Cell A{row} đã có font là in đậm hay chưa?", "result": False},
            {"item": f"Cell A{row} text đã ở vị trí center hay chưa?", "result": False},
            {"item": f"Cell A{row} đã merge các cột A -> BO vào hay chưa?", "result": False},
            {"item": f"Cell A{row} đã 「Thick Outside Borders」hay chưa?", "result": False},
        ]

        if "処理" in cell.value:
            checklist[0]["result"] = True
        if cell.font.size == 18:
            checklist[1]["result"] = True
        if cell.font.b:
            checklist[2]["result"] = True
        if cell.alignment.horizontal == "center":
            checklist[3]["result"] = True
        if f'A{row}:BO{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True
        if cell.border.bottom.style and cell.border.right.style:
            checklist[5]["result"] = True

        return checklist

    def tc_3(self, sheet):
        row = 3
        header_row = sheet[row]

        checklist = [
            {"item": f"Dòng {row}, đã có text「サブシステムID」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> G vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột H -> O vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「サブシステム名」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột P -> V vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột W -> AM vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「作成者」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AN -> AS vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột AT -> BA vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「作成日」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột BB -> BG vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột BH -> BO vào hay chưa?", "result": False},
        ]

        if header_row[0].value and "サブシステムID" in header_row[0].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if f'A{row}:G{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[0].fill.start_color.index == 22:
            checklist[3]["result"] = True
        if f'H{row}:O{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True

        if header_row[15].value and "サブシステム名" in header_row[15].value:
            checklist[5]["result"] = True
            checklist[6]["result"] = True
        if f'P{row}:V{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[7]["result"] = True
        if header_row[15].fill.start_color.index == 22:
            checklist[8]["result"] = True
        if f'W{row}:AM{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[9]["result"] = True

        if header_row[39].value and "作成者" in header_row[39].value:
            checklist[10]["result"] = True
            checklist[11]["result"] = True
        if f'AN{row}:AS{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[12]["result"] = True
        if header_row[39].fill.start_color.index == 22:
            checklist[13]["result"] = True
        if f'AT{row}:BA{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[14]["result"] = True

        if header_row[53].value and "作成日" in header_row[53].value:
            checklist[15]["result"] = True
            checklist[16]["result"] = True
        if f'BB{row}:BG{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[17]["result"] = True
        if header_row[53].fill.start_color.index == 22:
            checklist[18]["result"] = True
        if f'BH{row}:BO{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[19]["result"] = True

        return checklist

    def tc_4(self, sheet):
        row = 4
        header_row = sheet[row]

        checklist = [
            {"item": f"Dòng {row}, đã có text「管理ID」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> G vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột H -> O vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「サービス群名」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột P -> V vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột W -> AM vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「更新者」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AN -> AS vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột AT -> BA vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「更新日」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột BB -> BG vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột BH -> BO vào hay chưa?", "result": False},
        ]

        if header_row[0].value and "管理ID" in header_row[0].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if f'A{row}:G{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[0].fill.start_color.index == 22:
            checklist[3]["result"] = True
        if f'H{row}:O{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True

        if header_row[15].value and "サービス群名" in header_row[15].value:
            checklist[5]["result"] = True
            checklist[6]["result"] = True
        if f'P{row}:V{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[7]["result"] = True
        if header_row[15].fill.start_color.index == 22:
            checklist[8]["result"] = True
        if f'W{row}:AM{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[9]["result"] = True

        if header_row[39].value and "更新者" in header_row[39].value:
            checklist[10]["result"] = True
            checklist[11]["result"] = True
        if f'AN{row}:AS{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[12]["result"] = True
        if header_row[39].fill.start_color.index == 22:
            checklist[13]["result"] = True
        if f'AT{row}:BA{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[14]["result"] = True

        if header_row[53].value and "更新日" in header_row[53].value:
            checklist[15]["result"] = True
            checklist[16]["result"] = True
        if f'BB{row}:BG{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[17]["result"] = True
        if header_row[53].fill.start_color.index == 22:
            checklist[18]["result"] = True
        if f'BH{row}:BO{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[19]["result"] = True

        return checklist

    def tc_5(self, sheet):
        row = 5
        header_row = sheet[row]

        checklist = [
            {"item": f"Dòng {row}, đã có text「サービスID」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> G vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột H -> O vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「サービス名」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột P -> V vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột W -> AM vào hay chưa?", "result": False},
        ]

        if header_row[0].value and "サービスID" in header_row[0].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if f'A{row}:G{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[0].fill.start_color.index == 22:
            checklist[3]["result"] = True
        if f'H{row}:O{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True

        if header_row[15].value and "サービス名" in header_row[15].value:
            checklist[5]["result"] = True
            checklist[6]["result"] = True
        if f'P{row}:V{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[7]["result"] = True
        if header_row[15].fill.start_color.index == 22:
            checklist[8]["result"] = True
        if f'W{row}:AM{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[9]["result"] = True

        return checklist

    def tc_6(self, sheet):
        row = 7
        header_row = sheet[row]

        checklist = [
            {"item": f"Dòng {row}, đã có text「画面ID」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> G vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột H -> AH vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#FFCC99」 hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「画面名」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AI -> AN vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột AO -> BO vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#FFCC99」 hay chưa?", "result": False},
        ]

        if header_row[0].value and "画面ID" in header_row[0].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if f'A{row}:G{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[0].fill.start_color.index == 22:
            checklist[3]["result"] = True
        if f'H{row}:AH{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True
        if header_row[7].fill.start_color.index == 47:
            checklist[5]["result"] = True

        if header_row[34].value and "画面名" in header_row[34].value:
            checklist[6]["result"] = True
            checklist[7]["result"] = True
        if f'AI{row}:AN{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[8]["result"] = True
        if header_row[34].fill.start_color.index == 22:
            checklist[9]["result"] = True
        if f'AO{row}:BO{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[10]["result"] = True
        if header_row[40].fill.start_color.index == 47:
            checklist[11]["result"] = True

        return checklist

    def tc_7(self, sheet):
        row = 9
        header_row = sheet[row]

        checklist = [
            {"item": f"Dòng {row}, header đã có cột「No」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> B vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},

            {"item": f"Dòng {row}, header đã có cột「名称」 hay chưa? ", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột C -> D vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},

            {"item": f"Dòng {row}, đheader đã có cột「インタラクション／画面処理定義内容」 hay chưa? ", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột E -> BO vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
        ]

        if header_row[0].value and 'No' in header_row[0].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if f'A{row}:B{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[0].fill.start_color.index == 22:
            checklist[3]["result"] = True

        if header_row[2].value and "名称" in header_row[2].value:
            checklist[4]["result"] = True
            checklist[5]["result"] = True
        if f'C{row}:D{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[6]["result"] = True
        if header_row[2].fill.start_color.index == 22:
            checklist[7]["result"] = True

        if header_row[4].value and "インタラクション／画面処理定義内容" in header_row[4].value:
            checklist[8]["result"] = True
            checklist[9]["result"] = True
        if f'E{row}:BO{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[10]["result"] = True
        if header_row[4].fill.start_color.index == 22:
            checklist[11]["result"] = True

        return checklist

    def tc_8(self, sheet):
        row = 10
        header_row = sheet[row]
        checklist = [
            {"item": f"Dòng {row}, header đã có cột「インタラクションID／画面処理ID」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột E -> L vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},

            {"item": "Đã merge các cột M -> BO vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#FFCC99」 hay chưa?", "result": False},
        ]

        if header_row[4].value and 'インタラクションID／\n画面処理ID' in header_row[4].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if f'E{row}:L{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[4].fill.start_color.index == 22:
            checklist[3]["result"] = True

        if f'M{row}:BO{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True
        if header_row[12].fill.start_color.index == 47:
            checklist[5]["result"] = True

        return checklist

    def tc_9(self, sheet):
        row = 11
        header_row = sheet[row]
        checklist = [
            {"item": f"Dòng {row} -> {row + 3}, header đã có cột「関連チェック」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột E -> L vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},

            {"item": f"Dòng {row}, header đã có cột「チェック内容」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột M -> AQ vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},

            {"item": f"Dòng {row}, header đã có cột「Nơi thực hiện validation (View or ViewModel)」 hay chưa?",
             "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AR -> AY vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},

            {"item": f"Dòng {row}, header đã có cột「エラーメッセージID」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AZ -> BG vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},

            {"item": f"Dòng {row}, header đã có cột「ダイアログ-アイコン」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột BH -> BO vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
        ]

        if f'E{row}:L{row + 3}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
            if header_row[4].value and '関連チェック' in header_row[4].value:
                checklist[0]["result"] = True
                checklist[1]["result"] = True
        if header_row[4].fill.start_color.index == 22:
            checklist[3]["result"] = True

        if header_row[12].value and 'チェック内容' in header_row[12].value:
            checklist[4]["result"] = True
            checklist[5]["result"] = True
        if f'M{row}:AQ{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[6]["result"] = True
        if header_row[12].fill.start_color.index == 22:
            checklist[7]["result"] = True

        if header_row[43].value and 'Nơi thực hiện validation (View or ViewModel)' in header_row[43].value:
            checklist[8]["result"] = True
            checklist[9]["result"] = True
        if f'M{row}:AQ{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[10]["result"] = True
        if header_row[43].fill.start_color.index == 22:
            checklist[11]["result"] = True

        if header_row[51].value and 'エラーメッセージID' in header_row[51].value:
            checklist[12]["result"] = True
            checklist[13]["result"] = True
        if f'AZ{row}:BG{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[14]["result"] = True
        if header_row[51].fill.start_color.index == 22:
            checklist[15]["result"] = True

        if header_row[59].value and 'ダイアログ-アイコン' in header_row[59].value:
            checklist[16]["result"] = True
            checklist[17]["result"] = True
        if f'BH{row}:BO{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[18]["result"] = True
        if header_row[59].fill.start_color.index == 22:
            checklist[19]["result"] = True

        return checklist

    def tc_10(self, sheet):
        row = 12
        header_row = sheet[row]
        checklist = [
            {"item": f"Dòng {row} -> {row + 2}, đã merge các cột M -> AQ, AR -> AY, AZ -> BG, BH -> BO vào hay chưa?",
             "result": False},
            {"item": "Đã có background color là 「#FFCC99」 hay chưa?", "result": False},
        ]

        merge_result = True
        color_result = True
        for i in range(row, row + 3):
            if f'M{i}:AQ{i}' not in str(list(sheet.merged_cells.ranges)):
                merge_result = False
            if f'AR{i}:AY{i}' not in str(list(sheet.merged_cells.ranges)):
                merge_result = False
            if f'AZ{i}:BG{i}' not in str(list(sheet.merged_cells.ranges)):
                merge_result = False
            if f'BH{i}:BO{i}' not in str(list(sheet.merged_cells.ranges)):
                merge_result = False

            if sheet[i][12].fill.start_color.index != 47 \
                    or sheet[i][43].fill.start_color.index != 47 \
                    or sheet[i][51].fill.start_color.index != 47 \
                    or sheet[i][59].fill.start_color.index != 47:
                color_result = False

        checklist[0]["result"] = merge_result
        checklist[1]["result"] = color_result

        return checklist

    def tc_11(self, sheet):
        row = 15
        header_row = sheet[row]
        checklist = [
            {"item": f"Dòng {row} -> {row + 2}, header đã có cột「処理内容」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột E -> L vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row} -> {row + 2}, các cột M -> BO đã có background color là 「#FFCC99」 hay chưa?", "result": False},
        ]

        if header_row[4].value and '処理内容' in header_row[4].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if f'E{row}:L{row + 2}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[4].fill.start_color.index == 22:
            checklist[3]["result"] = True
        if sheet[row][12].fill.start_color.index == 47 \
                and sheet[row + 1][12].fill.start_color.index == 47 \
                and sheet[row + 2][12].fill.start_color.index == 47:
            checklist[4]["result"] = True

        return checklist

    def tc_12(self, sheet):
        row = 18
        header_row = sheet[row]
        checklist = [
            {"item": f"Dòng {row} -> {row + 3}, header đã có cột「遷移先」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột E -> L vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},

            {"item": f"Dòng {row}, header đã có cột「条件」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột M -> AI vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},

            {"item": f"Dòng {row}, header đã có cột「遷移先画面ID」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AJ -> AQ vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},

            {"item": f"Dòng {row}, header đã có cột「遷移先画面名」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AR -> AY vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},

            {"item": f"Dòng {row}, header đã có cột「メッセージID」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AZ -> BG vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},

            {"item": f"Dòng {row}, header đã có cột「ダイアログ-アイコン」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột BH -> BO vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
        ]

        if header_row[4].value and '遷移先' in header_row[4].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if f'E{row}:L{row + 3}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[4].fill.start_color.index == 22:
            checklist[3]["result"] = True

        if header_row[12].value and '条件' in header_row[12].value:
            checklist[4]["result"] = True
            checklist[5]["result"] = True
        if f'M{row}:AI{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[6]["result"] = True
        if header_row[12].fill.start_color.index == 22:
            checklist[7]["result"] = True

        if header_row[35].value and '遷移先画面ID' in header_row[35].value:
            checklist[8]["result"] = True
            checklist[9]["result"] = True
        if f'AJ{row}:AQ{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[10]["result"] = True
        if header_row[35].fill.start_color.index == 22:
            checklist[11]["result"] = True

        if header_row[43].value and '遷移先画面名' in header_row[43].value:
            checklist[12]["result"] = True
            checklist[13]["result"] = True
        if f'AR{row}:AY{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[14]["result"] = True
        if header_row[43].fill.start_color.index == 22:
            checklist[15]["result"] = True

        if header_row[51].value and 'メッセージID' in header_row[51].value:
            checklist[16]["result"] = True
            checklist[17]["result"] = True
        if f'AZ{row}:BG{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[18]["result"] = True
        if header_row[51].fill.start_color.index == 22:
            checklist[19]["result"] = True

        if header_row[59].value and 'ダイアログ-アイコン' in header_row[59].value:
            checklist[20]["result"] = True
            checklist[21]["result"] = True
        if f'BH{row}:BO{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[22]["result"] = True
        if header_row[59].fill.start_color.index == 22:
            checklist[23]["result"] = True

        return checklist
