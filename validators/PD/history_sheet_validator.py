from validators.base_validator import BaseValidator


class HistorySheetValidator(BaseValidator):
    def validate(self, workbook):
        sheet_name = "改訂履歴"
        if sheet_name not in workbook.sheetnames:
            return []

        sheet = workbook[sheet_name]
        checklist = self.tc_1(sheet)
        checklist += self.tc_2(sheet)
        checklist += self.tc_3(sheet)

        for item in checklist:
            item['sheet'] = sheet_name

        return checklist

    def tc_1(self, sheet):
        checklist = [
            {"item": "Font của cả sheet này đã là 「ＭＳ Ｐゴシック」 hay chưa?", "result": False},
            {"item": "Font size của cả sheet này đã là 11 hay chưa?", "result": False},
        ]

        if sheet['A1'].font.name == 'ＭＳ Ｐゴシック':
            checklist[0]["result"] = True

        if sheet['A1'].font.size == 11:
            checklist[1]["result"] = True

        return checklist

    def tc_2(self, sheet):
        cell_A1 = sheet["A1"]
        checklist = [
            {"item": "Cell A1 có nội dung là 「改訂履歴」 hay chưa?", "result": False},
            {"item": " Cell A1 đã được bôi đậm hay chưa?", "result": False},
        ]

        if "改訂履歴" in cell_A1.value:
            checklist[0]["result"] = True
        if cell_A1.font.b:
            checklist[1]["result"] = True

        return checklist

    def tc_3(self, sheet):
        header_row = sheet[2]

        checklist = [
            {"item": "Dòng 2 header đã có background color là 「#C0C0C0」 hay chưa?", "result": False},

            {"item": "Dòng 2, header đã có cột 「No」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> B vào hay chưa?", "result": False},

            {"item": "Dòng 2, header đã có cột 「版」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột C -> E vào hay chưa?", "result": False},

            {"item": "Dòng 2, header đã có cột 「改訂日」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột F -> K vào hay chưa?", "result": False},

            {"item": "Dòng 2, header đã có cột 「担当者」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột L -> Q vào hay chưa?", "result": False},

            {"item": "Dòng 2, header đã có cột 「改訂箇所」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột R -> AC vào hay chưa?", "result": False},

            {"item": "Dòng 2, header đã có cột 「改訂内容」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AD -> BF vào hay chưa?", "result": False},

            {"item": "Dòng 2, header đã có cột 「関連番号」 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột BG -> BM vào hay chưa?", "result": False},
        ]

        if header_row[0].fill.start_color.index == 22:
            checklist[0]["result"] = True

        if header_row[0].value and "No" in header_row[0].value:
            checklist[1]["result"] = True
            checklist[2]["result"] = True
        if 'A2:B2' in str(list(sheet.merged_cells.ranges)):
            checklist[3]["result"] = True

        if header_row[2].value and "版" in header_row[2].value:
            checklist[4]["result"] = True
            checklist[5]["result"] = True
        if 'C2:E2' in str(list(sheet.merged_cells.ranges)):
            checklist[6]["result"] = True

        if header_row[5].value and "改訂日" in header_row[5].value:
            checklist[7]["result"] = True
            checklist[8]["result"] = True
        if 'F2:K2' in str(list(sheet.merged_cells.ranges)):
            checklist[9]["result"] = True

        if header_row[11].value and "担当者" in header_row[11].value:
            checklist[10]["result"] = True
            checklist[11]["result"] = True
        if 'L2:Q2' in str(list(sheet.merged_cells.ranges)):
            checklist[12]["result"] = True

        if header_row[17].value and "改訂箇所" in header_row[17].value:
            checklist[13]["result"] = True
            checklist[14]["result"] = True
        if 'R2:AC2' in str(list(sheet.merged_cells.ranges)):
            checklist[15]["result"] = True

        if header_row[29].value and "改訂内容" in header_row[29].value:
            checklist[16]["result"] = True
            checklist[17]["result"] = True
        if 'AD2:BF2' in str(list(sheet.merged_cells.ranges)):
            checklist[18]["result"] = True

        if header_row[58].value and "関連番号" in header_row[58].value:
            checklist[19]["result"] = True
            checklist[20]["result"] = True
        if 'BG2:BM2' in str(list(sheet.merged_cells.ranges)):
            checklist[21]["result"] = True

        return checklist
