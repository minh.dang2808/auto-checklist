from validators.base_validator import BaseValidator


class ScreenItemSheetValidator(BaseValidator):
    def validate(self, workbook):
        sheet_name = "画面項目"
        if sheet_name not in workbook.sheetnames:
            return []

        sheet = workbook[sheet_name]
        checklist = [self.tc_1(sheet)]
        checklist += self.tc_2(sheet)
        checklist += self.tc_3(sheet)
        checklist += self.tc_4(sheet)
        checklist += self.tc_5(sheet)
        checklist += self.tc_6(sheet)
        checklist += self.tc_7(sheet)
        checklist += self.tc_8(sheet)

        for item in checklist:
            item['sheet'] = sheet_name

        return checklist

    def tc_1(self, sheet):
        result = False
        if sheet['A1'].font.name == 'ＭＳ Ｐゴシック':
            result = True

        return {
            "item": "Font của cả sheet này đã là 「ＭＳ Ｐゴシック」 hay chưa?",
            "result": result
        }

    def tc_2(self, sheet):
        row = 1
        cell = sheet[f"A{row}"]
        checklist = [
            {"item": f"Cell A{row} đã có text là 「画面項目」hay chưa?", "result": False},
            {"item": f"Cell A{row} đã có font size là 18 hay chưa?", "result": False},
            {"item": f"Cell A{row} đã có font là in đậm hay chưa?", "result": False},
            {"item": f"Cell A{row} text đã ở vị trí center hay chưa?", "result": False},
            {"item": f"Cell A{row} đã merge các cột A -> BP vào hay chưa?", "result": False},
            {"item": f"Cell A{row} đã 「Thick Outside Borders」hay chưa?", "result": False},
        ]

        if "画面項目" in cell.value:
            checklist[0]["result"] = True
        if cell.font.size == 18:
            checklist[1]["result"] = True
        if cell.font.b:
            checklist[2]["result"] = True
        if cell.alignment.horizontal == "center":
            checklist[3]["result"] = True
        if f'A{row}:BP{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True
        if cell.border.bottom.style and cell.border.right.style:
            checklist[5]["result"] = True

        return checklist

    def tc_3(self, sheet):
        row = 3
        header_row = sheet[row]

        checklist = [
            {"item": f"Dòng {row}, đã có text「サブシステムID」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> F vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột G -> M vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「サブシステム名」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột N -> S vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột T -> AU vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「作成者」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AV -> BA vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột BB -> BI vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「作成日」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột BJ -> BO vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột BP -> BW vào hay chưa?", "result": False},
        ]

        if header_row[0].value and "サブシステムID" in header_row[0].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if f'A{row}:F{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[0].fill.start_color.index == 22:
            checklist[3]["result"] = True
        if f'G{row}:M{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True

        if header_row[13].value and "サブシステム名" in header_row[13].value:
            checklist[5]["result"] = True
            checklist[6]["result"] = True
        if f'N{row}:S{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[7]["result"] = True
        if header_row[13].fill.start_color.index == 22:
            checklist[8]["result"] = True
        if f'T{row}:AU{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[9]["result"] = True

        if header_row[33].value and "作成者" in header_row[33].value:
            checklist[10]["result"] = True
            checklist[11]["result"] = True
        if f'AV{row}:BA{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[12]["result"] = True
        if header_row[33].fill.start_color.index == 22:
            checklist[13]["result"] = True
        if f'BB{row}:BI{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[14]["result"] = True

        if header_row[47].value and "作成日" in header_row[47].value:
            checklist[15]["result"] = True
            checklist[16]["result"] = True
        if f'BJ{row}:BO{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[17]["result"] = True
        if header_row[47].fill.start_color.index == 22:
            checklist[18]["result"] = True
        if f'BP{row}:BW{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[19]["result"] = True

        return checklist

    def tc_4(self, sheet):
        row = 4
        header_row = sheet[row]

        checklist = [
            {"item": f"Dòng {row}, đã có text「管理ID」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> F vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột G -> M vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「サービス群名」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột N -> S vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột T -> AU vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「更新者」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AV -> BA vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột BB -> BI vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「更新日」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột BJ -> BO vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột BP -> BW vào hay chưa?", "result": False},
        ]

        if header_row[0].value and "管理ID" in header_row[0].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if f'A{row}:F{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[0].fill.start_color.index == 22:
            checklist[3]["result"] = True
        if f'G{row}:M{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True

        if header_row[13].value and "サービス群名" in header_row[13].value:
            checklist[5]["result"] = True
            checklist[6]["result"] = True
        if f'N{row}:S{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[7]["result"] = True
        if header_row[13].fill.start_color.index == 22:
            checklist[8]["result"] = True
        if f'T{row}:AU{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[9]["result"] = True

        if header_row[33].value and "更新者" in header_row[33].value:
            checklist[10]["result"] = True
            checklist[11]["result"] = True
        if f'AV{row}:BA{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[12]["result"] = True
        if header_row[33].fill.start_color.index == 22:
            checklist[13]["result"] = True
        if f'BB{row}:BI{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[14]["result"] = True

        if header_row[47].value and "更新日" in header_row[47].value:
            checklist[15]["result"] = True
            checklist[16]["result"] = True
        if f'BJ{row}:BO{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[17]["result"] = True
        if header_row[47].fill.start_color.index == 22:
            checklist[18]["result"] = True
        if f'BP{row}:BW{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[19]["result"] = True

        return checklist

    def tc_5(self, sheet):
        row = 5
        header_row = sheet[row]

        checklist = [
            {"item": f"Dòng {row}, đã có text「サービスID」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> F vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột G -> M vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「サービス名」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột N -> S vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột T -> AU vào hay chưa?", "result": False},
        ]

        if header_row[0].value and "サービスID" in header_row[0].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if f'A{row}:F{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[0].fill.start_color.index == 22:
            checklist[3]["result"] = True
        if f'G{row}:M{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True

        if header_row[13].value and "サービス名" in header_row[13].value:
            checklist[5]["result"] = True
            checklist[6]["result"] = True
        if f'N{row}:S{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[7]["result"] = True
        if header_row[13].fill.start_color.index == 22:
            checklist[8]["result"] = True
        if f'T{row}:AU{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[9]["result"] = True

        return checklist

    def tc_6(self, sheet):
        row = 7
        header_row = sheet[row]

        checklist = [
            {"item": f"Dòng {row}, đã có text「画面ID」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột A -> F vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột G -> AP vào hay chưa?", "result": False},

            {"item": f"Dòng {row}, đã có text「画面名」hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AQ -> AV vào hay chưa?", "result": False},
            {"item": "Đã có background color là 「#C0C0C0」 hay chưa?", "result": False},
            {"item": f"Dòng {row}, đã merge các cột T -> AU vào hay chưa?", "result": False},
        ]

        if header_row[0].value and "画面ID" in header_row[0].value:
            checklist[0]["result"] = True
            checklist[1]["result"] = True
        if f'A{row}:AP{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[2]["result"] = True
        if header_row[0].fill.start_color.index == 22:
            checklist[3]["result"] = True
        if f'G{row}:M{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True

        if header_row[28].value and "画面名" in header_row[28].value:
            checklist[5]["result"] = True
            checklist[6]["result"] = True
        if f'AQ{row}:AV{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[7]["result"] = True
        if header_row[28].fill.start_color.index == 22:
            checklist[8]["result"] = True
        if f'AW{row}:BW{row}' in str(list(sheet.merged_cells.ranges)):
            checklist[9]["result"] = True

        return checklist

    def tc_7(self, sheet):
        row = 9
        cell = sheet[f"A{row}"]
        checklist = [
            {"item": f"Cell A{row} đã có text là 「<入出力項目>」hay chưa?", "result": False},
        ]

        if "<入出力項目>" in cell.value:
            checklist[0]["result"] = True

        return checklist

    def tc_8(self, sheet):
        row = 10
        header_row = sheet[row]
        header_next_row = sheet[row + 1]
        cell = sheet[f"A{row}"]
        next_cell = sheet[f"A{row + 1}"]

        checklist = [
            {"item": f"Dòng A{row} và A{row + 1}, header đã có background color là 「#C0C0C0」 hay chưa?", "result": False},

            {"item": f"Dòng A{row} và A{row + 1} header đã có cột là 「No」 hay chưa?", "result": False},
            {"item": f"Dòng A{row} và A{row + 1} đã merge các cột A -> B vào hay chưa?", "result": False},

            {"item": f"Dòng A{row} header đã có cột là 「項目論理名」 hay chưa?", "result": False},
            {"item": f"Dòng A{row} đã merge các cột C -> I vào hay chưa?", "result": False},
            {"item": f"Dòng A{row + 1} đã merge các cột C -> I vào hay chưa?", "result": False},

            {"item": f"Dòng A{row} header đã có cột là 「項目名」 hay chưa?", "result": False},
            {"item": f"Dòng A{row} đã merge các cột J -> P vào hay chưa?", "result": False},
            {"item": f"Dòng A{row + 1} đã merge các cột J -> P vào hay chưa?", "result": False},

            {"item": f"Dòng A{row} header đã có cột là 「In/Out」 hay chưa?", "result": False},
            {"item": f"Dòng A{row} đã merge các cột Q -> S vào hay chưa?", "result": False},
            {"item": f"Dòng A{row + 1} đã merge các cột Q -> S vào hay chưa?", "result": False},

            {"item": f"Dòng A{row} header đã có cột là 「タイプ」 hay chưa?", "result": False},
            {"item": f"Dòng A{row} đã merge các cột  T -> AA vào hay chưa?", "result": False},
            {"item": f"Dòng A{row + 1} đã merge các cột  T -> AA vào hay chưa?", "result": False},

            {"item": f"Dòng A{row} header đã có cột là 「最大桁数」 hay chưa?", "result": False},
            {"item": f"Dòng A{row} đã merge các cột AB -> AH vào hay chưa?", "result": False},
            {"item": f"Dòng A{row + 1} đã merge các cột AB -> AH vào hay chưa?", "result": False},

            {"item": f"Dòng A{row} header đã có cột là 「TABオーダー」 hay chưa?", "result": False},
            {"item": f"Dòng A{row} đã merge các cột AI -> AO vào hay chưa?", "result": False},
            {"item": f"Dòng A{row + 1} đã merge các cột AI -> AO vào hay chưa?", "result": False},

            {"item": f"Dòng A{row} header đã có cột là 「初期入力値」 hay chưa?", "result": False},
            {"item": f"Dòng A{row} đã merge các cột AP -> AV vào hay chưa?", "result": False},
            {"item": f"Dòng A{row + 1} đã merge các cột AP -> AV vào hay chưa?", "result": False},

            {"item": f"Dòng A{row} header đã có cột là 「対応するDB項目」 hay chưa?", "result": False},
            {"item": f"Dòng A{row} đã merge các cột AW -> BL vào hay chưa?", "result": False},

            {"item": f"Dòng A{row + 1} header đã có cột là 「テーブル論理名」 hay chưa?", "result": False},
            {"item": f"Dòng A{row + 1} đã merge các cột AW -> BD vào hay chưa?", "result": False},

            {"item": f"Dòng A{row + 1} header đã có cột là 「カラム論理名」 hay chưa?", "result": False},
            {"item": f"Dòng A{row + 1} đã merge các cột BE -> BL vào hay chưa?", "result": False},

            {"item": f"Dòng A{row} header đã có cột là 「インタラクション別入力項目」 hay chưa?", "result": False},
            {"item": f"Dòng A{row} đã merge các cột BM -> BX vào hay chưa?", "result": False},

            {"item": f"Dòng A{row} header đã có cột là 「インタラクションID」 hay chưa?", "result": False},
            {"item": f"Dòng A{row} đã merge các cột BY -> CF vào hay chưa?", "result": False},
            {"item": f"Dòng A{row + 1} đã merge các cột BY -> CF vào hay chưa?", "result": False},

            {"item": f"Dòng A{row} header đã có cột là 「Data Binding」 hay chưa?", "result": False},
            {"item": f"Dòng A{row} đã merge các cột CG -> CV vào hay chưa?", "result": False},
            {"item": f"Font color đã là 「#FF0000」hay chưa?", "result": False},

            {"item": f"Dòng A{row + 1} header đã có cột là 「VM item name」 hay chưa?", "result": False},
            {"item": f"Dòng A{row + 1} đã merge các cột CG -> CN vào hay chưa?", "result": False},
            {"item": f"Font color đã là 「#FF0000」hay chưa?", "result": False},

            {"item": f"Dòng A{row + 1} header đã có cột là 「Type」 hay chưa?", "result": False},
            {"item": f"Dòng A{row + 1} đã merge các cột CO -> CV vào hay chưa?", "result": False},
            {"item": f"Font color đã là 「#FF0000」hay chưa?", "result": False},

            {"item": f"Dòng A{row} header đã có cột là 「Command Binding」 hay chưa?", "result": False},
            {"item": f"Dòng A{row} đã merge các cột CW -> DL vào hay chưa?", "result": False},
            {"item": f"Font color đã là 「#FF0000」hay chưa?", "result": False},

            {"item": f"Dòng A{row + 1} header đã có cột là 「VM item name」 hay chưa?", "result": False},
            {"item": f"Dòng A{row + 1} đã merge các cột CW -> DD vào hay chưa?", "result": False},
            {"item": f"Font color đã là 「#FF0000」hay chưa?", "result": False},

            {"item": f"Dòng A{row + 1} header đã có cột là 「Type」 hay chưa?", "result": False},
            {"item": f"Dòng A{row + 1} đã merge các cột DE -> DL vào hay chưa?", "result": False},
            {"item": f"Font color đã là 「#FF0000」hay chưa?", "result": False},

            {"item": f"Dòng A{row} header đã có cột là 「備考」 hay chưa?", "result": False},
            {"item": f"Dòng A{row} đã merge các cột DM -> ED vào hay chưa?", "result": False},
            {"item": f"Dòng A{row + 1} đã merge các cột DM -> ED vào hay chưa?", "result": False},
        ]

        if f"A{row}:B{row + 1}" in str(list(sheet.merged_cells.ranges)):
            if cell.fill.start_color.index == 22:
                checklist[0]["result"] = True
            if cell.value and "No" in cell.value:
                checklist[1]["result"] = True
            checklist[2]["result"] = True

        if header_row[2].value and "項目論理名" in header_row[2].value:
            checklist[3]["result"] = True
        if f"C{row}:I{row}" in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True
        if f"C{row + 1}:I{row + 1}" in str(list(sheet.merged_cells.ranges)):
            checklist[5]["result"] = True

        if header_row[2].value and "項目名" in header_row[2].value:
            checklist[6]["result"] = True
        if f"J{row}:P{row}" in str(list(sheet.merged_cells.ranges)):
            checklist[7]["result"] = True
        if f"J{row + 1}:P{row + 1}" in str(list(sheet.merged_cells.ranges)):
            checklist[8]["result"] = True

        if header_row[2].value and "In/Out" in header_row[2].value:
            checklist[9]["result"] = True
        if f"Q{row}:S{row}" in str(list(sheet.merged_cells.ranges)):
            checklist[10]["result"] = True
        if f"Q{row + 1}:S{row + 1}" in str(list(sheet.merged_cells.ranges)):
            checklist[11]["result"] = True

        if header_row[2].value and "タイプ" in header_row[2].value:
            checklist[12]["result"] = True
        if f"T{row}:AA{row}" in str(list(sheet.merged_cells.ranges)):
            checklist[13]["result"] = True
        if f"T{row + 1}:AA{row + 1}" in str(list(sheet.merged_cells.ranges)):
            checklist[14]["result"] = True

        if header_row[2].value and "最大桁数" in header_row[2].value:
            checklist[15]["result"] = True
        if f"AB{row}:AH{row}" in str(list(sheet.merged_cells.ranges)):
            checklist[16]["result"] = True
        if f"AB{row + 1}:AH{row + 1}" in str(list(sheet.merged_cells.ranges)):
            checklist[17]["result"] = True

        if header_row[2].value and "TABオーダー" in header_row[2].value:
            checklist[18]["result"] = True
        if f"AI{row}:AO{row}" in str(list(sheet.merged_cells.ranges)):
            checklist[19]["result"] = True
        if f"AI{row + 1}:AO{row + 1}" in str(list(sheet.merged_cells.ranges)):
            checklist[20]["result"] = True

        if header_row[2].value and "TABオーダー" in header_row[2].value:
            checklist[21]["result"] = True
        if f"AP{row}:AV{row}" in str(list(sheet.merged_cells.ranges)):
            checklist[22]["result"] = True
        if f"AP{row + 1}:AV{row + 1}" in str(list(sheet.merged_cells.ranges)):
            checklist[23]["result"] = True

        if header_row[2].value and "対応するDB項目" in header_row[2].value:
            checklist[24]["result"] = True
        if f"AW{row}:BL{row}" in str(list(sheet.merged_cells.ranges)):
            checklist[25]["result"] = True

        if header_next_row[2].value and "テーブル論理名" in header_next_row[2].value:
            checklist[26]["result"] = True
        if f"AW{row}:BD{row}" in str(list(sheet.merged_cells.ranges)):
            checklist[27]["result"] = True

        if header_next_row[2].value and "カラム論理名" in header_next_row[2].value:
            checklist[28]["result"] = True
        if f"BE{row}:BL{row}" in str(list(sheet.merged_cells.ranges)):
            checklist[29]["result"] = True

        if header_row[2].value and "インタラクションID" in header_row[2].value:
            checklist[30]["result"] = True
        if f"BY{row}:CF{row}" in str(list(sheet.merged_cells.ranges)):
            checklist[31]["result"] = True
        if f"BY{row + 1}:CF{row + 1}" in str(list(sheet.merged_cells.ranges)):
            checklist[32]["result"] = True

        if header_row[2].value and "Data Binding" in header_row[2].value:
            checklist[33]["result"] = True
        if f"CG{row}:CV{row}" in str(list(sheet.merged_cells.ranges)):
            checklist[34]["result"] = True
        if sheet[f'CG{row}'].font.color and "FF0000" in sheet[f'CG{row}'].font.color.index:
            checklist[35]["result"] = True

        if header_next_row[2].value and "VM item name" in header_next_row[2].value:
            checklist[36]["result"] = True
        if f"CG{row + 1}:CN{row + 1}" in str(list(sheet.merged_cells.ranges)):
            checklist[37]["result"] = True
        if sheet[f'CG{row + 1}'].font.color and "FF0000" in sheet[f'CG{row + 1}'].font.color.index:
            checklist[38]["result"] = True

        if header_next_row[2].value and "Type" in header_next_row[2].value:
            checklist[39]["result"] = True
        if f"CO{row + 1}:CV{row + 1}" in str(list(sheet.merged_cells.ranges)):
            checklist[40]["result"] = True
        if sheet[f'CO{row + 1}'].font.color and "FF0000" in sheet[f'CO{row + 1}'].font.color.index:
            checklist[41]["result"] = True

        if header_row[2].value and "Command Binding" in header_row[2].value:
            checklist[42]["result"] = True
        if f"CW{row}:DL{row}" in str(list(sheet.merged_cells.ranges)):
            checklist[43]["result"] = True
        if sheet[f'CW{row}'].font.color and "FF0000" in sheet[f'CW{row}'].font.color.index:
            checklist[44]["result"] = True

        if header_next_row[2].value and "VM item name" in header_next_row[2].value:
            checklist[45]["result"] = True
        if f"CW{row + 1}:DD{row + 1}" in str(list(sheet.merged_cells.ranges)):
            checklist[46]["result"] = True
        if sheet[f'CW{row + 1}'].font.color and "FF0000" in sheet[f'CW{row + 1}'].font.color.index:
            checklist[47]["result"] = True

        if header_next_row[2].value and "Type" in header_next_row[2].value:
            checklist[48]["result"] = True
        if f"DE{row + 1}:DL{row + 1}" in str(list(sheet.merged_cells.ranges)):
            checklist[49]["result"] = True
        if sheet[f'DE{row + 1}'].font.color and "FF0000" in sheet[f'DE{row + 1}'].font.color.index:
            checklist[50]["result"] = True

        if header_row[2].value and "備考" in header_row[2].value:
            checklist[51]["result"] = True
        if f"DM{row}:ED{row}" in str(list(sheet.merged_cells.ranges)):
            checklist[52]["result"] = True
        if f"DM{row + 1}:ED{row + 1}" in str(list(sheet.merged_cells.ranges)):
            checklist[53]["result"] = True

        return checklist
