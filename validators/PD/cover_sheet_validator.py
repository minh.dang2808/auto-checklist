from validators.base_validator import BaseValidator


class CoverSheetValidator(BaseValidator):
    def validate(self, workbook):
        sheet_name = "表紙"
        if sheet_name not in workbook.sheetnames:
            return []

        sheet = workbook[sheet_name]
        checklist = [self.tc_1(sheet)]
        checklist += self.tc_2(sheet)
        checklist += self.tc_3(sheet)
        checklist += self.tc_4(sheet)
        checklist += self.tc_5(sheet)
        checklist += self.tc_6(sheet)

        for item in checklist:
            item['sheet'] = sheet_name

        return checklist

    def tc_1(self, sheet):
        result = False
        if sheet['A1'].font.name == 'ＭＳ Ｐゴシック':
            result = True

        return {
            "item": "Font của cả sheet này đã là 「ＭＳ Ｐゴシック」 hay chưa?",
            "result": result
        }

    def tc_2(self, sheet):
        cell_A6 = sheet["A6"]
        checklist = [
            {"item": "Cell A6 đã có text là 「画面設計書」 hay chưa?", "result": False},
            {"item": "Cell A6 đã có font size là 36 hay chưa?", "result": False},
            {"item": "Cell A6 đã có font là in đậm hay chưa?", "result": False},
            {"item": "Cell A6 text đã ở vị trí center hay chưa?", "result": False},
            {"item": "Cell A6 đã merge các cột A -> BL vào hay chưa?", "result": False},
        ]

        if cell_A6.value == "画面設計書":
            checklist[0]["result"] = True
        if cell_A6.font.size == 36:
            checklist[1]["result"] = True
        if cell_A6.font.b:
            checklist[2]["result"] = True
        if cell_A6.alignment.horizontal == "center":
            checklist[3]["result"] = True
        if 'A6:BL6' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True

        return checklist

    def tc_3(self, sheet):
        cell_A24 = sheet["A24"]
        checklist = [
            {"item": "Cell A24 đã có công thức link đến sheet 「改訂履歴」 hay chưa?", "result": False},
            {"item": "Cell A24 đã có nội dung là 「Phiên bản X.X」 hay chưa?", "result": False},
            {"item": "Cell A24 đã có font size là 24 hay chưa?", "result": False},
            {"item": "Cell A24 đã có font không là in đậm hay chưa?", "result": False},
            {"item": "Cell A24 text đã ở vị trí center hay chưa?", "result": False},
            {"item": " Cell A24 đã merge các cột A -> BL vào hay chưa?", "result": False},
        ]

        if cell_A24.value in '=IF(COUNTA(改訂履歴!C3:E22)>0,"第"&OFFSET(改訂履歴!C3,COUNTA(改訂履歴!C3:E22)-1,0)&"版","第0.0版")':
            checklist[0]["result"] = True
        # if cell_A24.value == "Phiên bản X.X":
            checklist[1]["result"] = True
        if cell_A24.font.size == 24:
            checklist[2]["result"] = True
        if not cell_A24.font.b:
            checklist[3]["result"] = True
        if cell_A24.alignment.horizontal == "center":
            checklist[4]["result"] = True
        if 'A24:BL24' in str(list(sheet.merged_cells.ranges)):
            checklist[5]["result"] = True

        return checklist

    def tc_4(self, sheet):
        cell_A26 = sheet["A26"]
        checklist = [
            {"item": "Cell A26 đã có nội dung là 「YYYY/MM/DD」 hay chưa?", "result": False},
            {"item": "Cell A26 đã có font size là 24 hay chưa?", "result": False},
            {"item": "Cell A26 đã có font là in đậm hay chưa?", "result": False},
            {"item": "Cell A26 text đã ở vị trí center hay chưa?", "result": False},
            {"item": " Cell A26 đã merge các cột A -> BL vào hay chưa?", "result": False},
        ]

        if cell_A26.value in '=IF(COUNTA(改訂履歴!F3:K22)>0,OFFSET(改訂履歴!F3,COUNTA(改訂履歴!F3:K22)-1,0),"1900/01/01")':
            checklist[0]["result"] = True
        if cell_A26.font.size == 24:
            checklist[1]["result"] = True
        if cell_A26.font.b:
            checklist[2]["result"] = True
        if cell_A26.alignment.horizontal == "center":
            checklist[3]["result"] = True
        if 'A26:BL26' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True

        return checklist

    def tc_5(self, sheet):
        cell_A28 = sheet["A28"]
        checklist = [
            {"item": "Cell A28 đã có text là 「 BIPROGY株式会社」 hay chưa?", "result": False},
            {"item": "Cell A28 đã có font size là 24 hay chưa?", "result": False},
            {"item": "Cell A28 đã có font là không in đậm hay chưa?", "result": False},
            {"item": "Cell A28 text đã ở vị trí center hay chưa?", "result": False},
            {"item": " Cell A28 đã merge các cột A -> BL vào hay chưa?", "result": False},
        ]

        if 'BIPROGY株式会社' in cell_A28.value:
            checklist[0]["result"] = True
        if cell_A28.font.size == 24:
            checklist[1]["result"] = True
        if not cell_A28.font.bold:
            checklist[2]["result"] = True
        if cell_A28.alignment.horizontal == "center":
            checklist[3]["result"] = True
        if 'A28:BL28' in str(list(sheet.merged_cells.ranges)):
            checklist[4]["result"] = True

        return checklist

    def tc_6(self, sheet):
        header_row = sheet[31]
        required_columns = ["承認", "審査", "作成"]
        checklist = [
            {"item": "Dòng 31 header đã có cột 承認 hay chưa?", "result": False},
            {"item": "Có font size là 10 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột AZ -> BC vào hay chưa?", "result": False},
            {"item": "Dòng 31 header đã có cột 審査 hay chưa?", "result": False},
            {"item": "Có font size là 10 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột BD -> BG vào hay chưa?", "result": False},
            {"item": "Dòng 31 header đã có cột 作成 hay chưa?", "result": False},
            {"item": "Có font size là 10 hay chưa?", "result": False},
            {"item": "Có hiển thị hết không?", "result": False},
            {"item": "Đã merge các cột BH -> BK vào hay chưa?", "result": False},
        ]

        if "承認" in header_row[51].value:
            checklist[0]["result"] = True
            checklist[2]["result"] = True
        if header_row[51].font.size == 10:
            checklist[1]["result"] = True
        if "AZ31:BC31" in str(list(sheet.merged_cells.ranges)):
            checklist[3]["result"] = True

        if "審査" in header_row[55].value:
            checklist[4]["result"] = True
            checklist[6]["result"] = True
        if header_row[55].font.size == 10:
            checklist[5]["result"] = True
        if "BD31:BG31" in str(list(sheet.merged_cells.ranges)):
            checklist[7]["result"] = True

        if "作成" in header_row[59].value:
            checklist[8]["result"] = True
            checklist[10]["result"] = True
        if header_row[59].font.size == 10:
            checklist[9]["result"] = True
        if "BH31:BK31" in str(list(sheet.merged_cells.ranges)):
            checklist[11]["result"] = True

        return checklist
