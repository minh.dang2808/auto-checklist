from validators.base_validator import BaseValidator


class CoverSheetValidator(BaseValidator):
    def validate(self, workbook, file_name):
        sheet_name = "Cover"
        if sheet_name not in workbook.sheetnames:
            return []

        sheet = workbook[sheet_name]
        checklist = self.tc_1(sheet)
        checklist += self.tc_2(sheet)
        checklist += self.tc_3(sheet)

        for item in checklist:
            item['sheet'] = sheet_name

        return checklist

    def tc_1(self, sheet):
        checklist = [
            {"item": "Ô cell A6: font ＭＳ Ｐゴシック hay chưa?", "result": False},
            {"item": "Cell A6 đã có kiểu in đậm hay chưa?", "result": False},
            {"item": "Ô cell A6: size 36 hay chưa?", "result": False},
        ]

        if sheet['A6'].font.name == 'ＭＳ Ｐゴシック':
            checklist[0]["result"] = True

        if sheet['A6'].font.b:
            checklist[1]["result"] = True

        if sheet['A6'].font.size == 36:
            checklist[2]["result"] = True

        return checklist

    def tc_2(self, sheet):
        cell = sheet["A24"]
        checklist = [
            {"item": "Cell A24 đã có font là Times New Roman hay chưa?", "result": False},
            {"item": "Cell A24 đã có kiểu chữ in đậm hay chưa?", "result": False},
            {"item": "Cell A24 đã có font size là 24 hay chưa?", "result": False},
            {"item": "Cell A24: Đã có reference đến sheet 'Lịch sử thay đổi' hay chưa?", "result": False},
        ]

        if cell.font.name == 'Times New Roman':
            checklist[0]["result"] = True

        if cell.font.b:
            checklist[1]["result"] = True

        if cell.font.size == 24:
            checklist[2]["result"] = True

        if cell.value in '=IF(COUNTA(\'Lịch sử thay đổi\'!C3:E32)>0,"Phiên bản "&OFFSET(\'Lịch sử thay đổi\'!C3,COUNTA(\'Lịch sử thay đổi\'!C3:E32)-1,0),"Phiên bản 0.0")':
            checklist[3]["result"] = True

        return checklist

    def tc_3(self, sheet):
        cell = sheet["A26"]
        checklist = [
            {"item": "Cell A26 đã có font là Times New Roman hay chưa?", "result": False},
            {"item": "Cell A26 đã có kiểu chữ in đậm hay chưa?", "result": False},
            {"item": "Cell A26 đã có font size là 24 hay chưa?", "result": False},
            {"item": "Cell A26: Đã có reference đến sheet 'Lịch sử thay đổi' hay chưa?", "result": False},
            {"item": "Ô cell A26 đã có format YYYY/MM/DD hay chưa?", "result": False},
        ]

        if cell.font.name == 'Times New Roman':
            checklist[0]["result"] = True

        if cell.font.b:
            checklist[1]["result"] = True

        if cell.font.size == 24:
            checklist[2]["result"] = True

        if cell.value in '=IF(COUNTA(\'Lịch sử thay đổi\'!F3:J32)>0,OFFSET(\'Lịch sử thay đổi\'!F3,COUNTA(\'Lịch sử thay đổi\'!F3:J32)-1,0),"1900/01/01")':
            checklist[3]["result"] = True
            checklist[4]["result"] = True

        return checklist
