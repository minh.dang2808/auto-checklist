from validators.base_validator import BaseValidator


class EvidenceSheetValidator(BaseValidator):
    def validate(self, workbook, sheet_name):
        if sheet_name not in workbook.sheetnames:
            return []

        sheet = workbook[sheet_name]
        checklist = self.tc_1(sheet)
        checklist += self.tc_2(sheet)

        for item in checklist:
            item['sheet'] = sheet_name

        return checklist

    def tc_1(self, sheet):
        checklist = [
            {"item": "Sheet đã có font Times New Roman chưa?", "result": False},
            {"item": "Sheet đã có font size là 11 hay chưa?", "result": False},
        ]

        if sheet['S50'].font.name == 'Times New Roman':
            checklist[0]["result"] = True

        if sheet['S50'].font.size == 11:
            checklist[1]["result"] = True

        return checklist

    def tc_2(self, sheet):
        checklist = [
            {"item": "A3 đã có nội dung \"Test step\" chưa?", "result": False},
            {"item": 'A3 đã có kiểu chữ in đậm chưa?', "result": False},
            {"item": "Sheet đã có đủ 2 cột hay chưa?", "result": False},
            {"item": "A1 đã có nội dung \"Kết quả thực thi\" hay chưa?", "result": False},
            {"item": 'Cell N1 đã có nội dung "Kết quả kỳ vọng" hay chưa?', "result": False},
            {"item": 'Các test step đã đúng với test case hay chưa?', "result": True},
        ]

        if sheet["A3"].value and "Test step" in sheet["A3"].value:
            checklist[0]["result"] = True

        if sheet["A3"].font.b:
            checklist[1]["result"] = True

        if sheet["A1"].value and sheet["N1"].value:
            checklist[2]["result"] = True

        if sheet["A1"].value and "実施結果" in str(sheet["A1"].value):
            checklist[3]["result"] = True

        if sheet["N1"].value and "期待結果" in str(sheet["N1"].value):
            checklist[4]["result"] = True

        return checklist
