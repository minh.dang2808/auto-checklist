from validators.base_validator import BaseValidator


class CategorySheetValidator(BaseValidator):
    def validate(self, workbook, file_name):
        sheet_name = "Danh mục"
        if sheet_name not in workbook.sheetnames:
            return []

        sheet = workbook[sheet_name]
        checklist = self.tc_1(sheet)
        checklist += self.tc_2(sheet)

        for item in checklist:
            item['sheet'] = sheet_name

        return checklist

    def tc_1(self, sheet):
        checklist = [
            {"item": "Ô cell A1: font Times New Roman hay chưa?", "result": False},
            {"item": "Ô cell A1: size 24 hay chưa?", "result": False},
            {"item": "Sheet đã có font size là 11 hay chưa?", "result": False},
        ]

        if sheet['A1'].font.name == 'Times New Roman':
            checklist[0]["result"] = True

        if sheet['A1'].font.size == 24:
            checklist[1]["result"] = True

        if sheet['S50'].font.size == 11:
            checklist[2]["result"] = True

        return checklist

    def tc_2(self, sheet):
        checklist = [
            {"item": "Cell A3 đã có nội dung \"ID Sub system\" hay chưa?", "result": False},
            {"item": "A4 đã có nội dung \"ID quản lý\" hay chưa?", "result": False},
            {"item": "A5 đã có nội dung \"ID chức năng\" hay chưa?", "result": False},
            {"item": "D3 đã có nội dung \"Tên Sub System\" hay chưa?", "result": False},
            {"item": "D4 đã có nội dung \"Tên nhóm chức năng\" hay chưa?", "result": False},
            {"item": "D5 đã có nội dung \"Tên chức năng\" hay chưa?", "result": False},
            {"item": "G3 đã có nội dung \"Người tạo\" hay chưa?", "result": False},
            {"item": "G4 đã có nội dung \"Người cập nhật\" hay chưa?", "result": False},
            {"item": "J3 đã có nội dung \"Ngày tạo\" hay chưa?", "result": False},
            {"item": "J3 đã có nội dung \"Ngày cập nhật\" hay chưa?", "result": False},
            {"item": "Sheet đã có bảng Test case ID hay chưa?", "result": False},
        ]

        if sheet["A3"].value and "ID Sub system" in sheet["A3"].value:
            checklist[0]["result"] = True

        if sheet["A4"].value and "ID quản lý" in sheet["A4"].value:
            checklist[1]["result"] = True

        if sheet["A5"].value and "ID chức năng" in sheet["A5"].value:
            checklist[2]["result"] = True

        if sheet["D3"].value and "Tên Sub System" in sheet["D3"].value:
            checklist[3]["result"] = True

        if sheet["D4"].value and "Tên nhóm chức năng" in sheet["D4"].value:
            checklist[4]["result"] = True

        if sheet["D5"].value and "Tên chức năng" in sheet["D5"].value:
            checklist[5]["result"] = True

        if sheet["G3"].value and "Người tạo" in sheet["G3"].value:
            checklist[6]["result"] = True

        if sheet["G4"].value and "Người cập nhật" in sheet["G4"].value:
            checklist[7]["result"] = True

        if sheet["J3"].value and "Ngày tạo" in sheet["J3"].value:
            checklist[8]["result"] = True

        if sheet["J4"].value and "Ngày cập nhật" in sheet["J4"].value:
            checklist[9]["result"] = True

        if sheet["A7"].value and "Test case ID" in sheet["A7"].value:
            checklist[10]["result"] = True

        for item in sheet['A8:A500']:
            if item[0].value:
                checklist.append({
                    "item": f"Test case {item[0].value} đã link tới các sheet evidence tương ứng hay chưa?",
                    "result": bool(sheet["A8"].value and sheet["A8"].hyperlink and sheet["A8"].hyperlink.location)
                })

        return checklist
