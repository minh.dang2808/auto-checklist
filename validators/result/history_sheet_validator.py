from validators.base_validator import BaseValidator


class HistorySheetValidator(BaseValidator):
    def validate(self, workbook, file_name):
        sheet_name = "Lịch sử thay đổi"
        if sheet_name not in workbook.sheetnames:
            return []

        sheet = workbook[sheet_name]
        checklist = self.tc_1(sheet)
        checklist += self.tc_2(sheet)

        for item in checklist:
            item['sheet'] = sheet_name

        return checklist

    def tc_1(self, sheet):
        checklist = [
            {"item": "Font của cả sheet này đã là Times New Roman hay chưa?", "result": False},
            {"item": "Font size của cả sheet này đã là 11 hay chưa?", "result": False},
        ]

        if sheet['A1'].font.name == 'Times New Roman':
            checklist[0]["result"] = True

        if sheet['A1'].font.size == 11:
            checklist[1]["result"] = True

        return checklist

    def tc_2(self, sheet):
        row = 3
        checklist = [
            {"item": "Tại mỗi dòng đã có đủ thông tin:\n"
                     "- STT\n"
                     "- Phiên bản\n"
                     "- Ngày thay đổi\n"
                     "- Người thay đổi\n"
                     "- Nội dung thay đổi\n"
                     "- Sheet (optional)", "result": False},
        ]

        if sheet[f"A{row}"] and sheet[f"C{row}"] and sheet[f"F{row}"] and sheet[f"L{row}"] and sheet[f"R{row}"] and sheet[f"AD{row}"]:
            checklist[0]["result"] = True

        return checklist
